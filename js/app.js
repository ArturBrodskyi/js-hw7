function filterBy(arr, dataType) {
    const filtered = arr.filter(item => typeof item === dataType);
    console.log(filtered);
  }

  const arr = ['string', 25, '25', true, null]
  filterBy(arr, 'string')